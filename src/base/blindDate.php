<!DOCTYPE html>
<html lang="en">
<head>
    <title>My projects</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<!--NAVBAR-->
<nav class="navbar navbar-expand-sm bg-secondary fixed-top">
    <a class="navbar-brand colorNavbar" href="#">Navbar</a>
    <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon "></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ">
            <li class="nav-item ">
                <a class="nav-link whiteLetters" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link dOrange" href="frontEnd.php">Front-end</a>
            </li>
            <li class="nav-item">
                <a class="nav-link whiteLetters" href="backEnd.php">Back-end</a>
            </li>
        </ul>
    </div>
</nav>

<!--CONTAINER-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <nav class="navbar bg-secondary ">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="selectNumber.php">Losowanie liczby 1-9</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="memory.php">Memory</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="mushroomPicking.php">Zbieranie grzybów</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dOrange" href="#">Randka w ciemno</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="wordSearch.php">Szukanie słowa</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-sm-10 ">
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">js</div>
            </div>
            <div class="bg-danger text-light p-3">
                <h3>Podaj pięć imion damskich  oraz pięć imion męskich aby rozpocząć losowanie.</h3>
                <div class="row">
                    <form class="col text-justify" id="formularz" name="myForm" >
                        <label for="name1">
                            <strong>Imię kobiety:</strong>
                        </label><br>
                        <input id="name1" type="text" name="woman" value="" ><span id="comment1"></span>
                        <br><br>
                        <label for="name2">
                            <strong>Imię mężczyzny:</strong>
                        </label><br>
                        <input id="name2" type="text" name="man" value="" ><span id="comment2"></span>
                        <br><br>
                        <button class="btn btn-deafult" id="login" type="submit" value="Submit" >Wyślij</button>
                    </form>
                    <p class="col-6" id="demo"></p>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../js/blindDate.js"></script>

</body>
</html>
