<!DOCTYPE html>
<html lang="en">
<head>
    <title>My projects - comment</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<!--NAVBAR-->
<nav class="navbar navbar-expand-sm bg-secondary fixed-top">
    <a class="navbar-brand colorNavbar" href="#">Navbar</a>
    <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon "></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ">
            <li class="nav-item ">
                <a class="nav-link whiteLetters" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link whiteLetters" href="frontEnd.php">Front-end</a>
            </li>
            <li class="nav-item">
                <a class="nav-link dOrange" href="backEnd.php">Back-end</a>
            </li>
        </ul>
    </div>
</nav>

<!--CONTAINER-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <nav class="navbar bg-secondary ">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="bakery.html">Piekarnia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="form.html">Zapis do pliku</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dOrange" href="#">Wyślij e-mail</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-sm-10">
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
            <h1>Komentarz klienta</h1>
            <p>Proszę przekazać nam swoje komentarze</p>

            <form action="processTheComment.php " method="post">
                <p>Nazwisko:<br/>
                    <input type="text" name="name" size="40"/></p>

                <p>Adres poczty elektronicznej:<br/>
                    <input type="text" name="email" size="40"/></p>

                <p>Komentarz:<br/>
                    <textarea name="comment" cols="40" rows="8" ></textarea></p>

                <p><input type="submit" value="Wyślij komentarz" /></p>
            </form>
        </div>
    </div>
</div>

</body>
</html>
