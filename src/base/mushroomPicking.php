<!DOCTYPE html>
<html lang="en">
<head>
    <title>My projects</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<!--NAVBAR-->
<nav class="navbar navbar-expand-sm bg-secondary fixed-top">
    <a class="navbar-brand colorNavbar" href="#">Navbar</a>
    <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon "></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ">
            <li class="nav-item ">
                <a class="nav-link whiteLetters" href="../base/index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link dOrange" href="../base/frontEnd.php">Front-end</a>
            </li>
            <li class="nav-item">
                <a class="nav-link whiteLetters" href="../base/backEnd.php">Back-end</a>
            </li>
        </ul>
    </div>
</nav>

<!--CONTAINER-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <nav class="navbar bg-secondary ">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="selectNumber.php">Losowanie liczby 1-9</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="memory.php">Memory</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dOrange" href="#">Zbieranie grzybów</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="blindDate.php">Randka w ciemno</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="wordSearch.php">Szukanie słowa</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-sm-10">
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2  ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">js</div>
            </div>
            <div  id="content2">
                <!--Masz 6 rodzajów grzybów w tym muchomor.Losujesz pięć. Grzyby podczas losowania nie mogą sie powtarzać. Jeżeli natrafisz-->
                <!--na muchomora kończysz zbieranie.-->
                <button id="myBtn" type="button" class="btn btn-success" onclick="myFunction()">
                    Kliknij aby zacząć zbierać grzyby !
                </button>
                <button id="info" type="button" class="btn btn-info d-none" onclick="myFunction1()">Spróbuj jeszcze raz</button>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Zebrane grzyby:<span id="how_many_gathered"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="mushrooms">brak grzybów</td>
                        </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>

<script src="../js/mushroomPicking.js"></script>

</body>
</html>
