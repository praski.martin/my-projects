<!DOCTYPE html>
<html lang="en">
<head>
    <title>My projects</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<!--NAVBAR-->
<nav class="navbar navbar-expand-sm bg-secondary fixed-top">
    <a class="navbar-brand colorNavbar" href="#">Navbar</a>
    <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon "></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ">
            <li class="nav-item ">
                <a class="nav-link whiteLetters" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link whiteLetters" href="frontEnd.php">Front-end</a>
            </li>
            <li class="nav-item">
                <a class="nav-link dOrange" href="backEnd.php">Back-end</a>
            </li>
        </ul>
    </div>
</nav>

<!--CONTAINER-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <nav class="navbar bg-secondary ">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="bakery.html">Piekarnia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="form.html">Zapis do pliku</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dOrange" href="comment.php">Wyślij e-mail</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-sm-10">
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
            <?php
            //UTWORZENIE KRÓTKICH NAZW ZMIENNYCH
            $name = trim($_POST['name']);  //trim usuwa znaki odstępu z początku i końca łańcucha
            $email = trim($_POST['email']);
            $comment = trim($_POST['comment']);

            //ZDEFINIOWANIE DANYCH STATYCZNYCH
            $addressTo = "praski.martin@gmail.com";
            $topic = "Komentarz ze strony WWW";
            $content = "Nazwa klienta: ".$name."\n".
                "Adres pocztowy: ".$email."\n".
                "Komentarz klienta: \n".str_replace("\r\n","",$comment)."\n"; //czysci tekst - zamienia \r\n na ""

            mail($addressTo, $topic, $content);
            ?>
            <h1>Komentarz przyjęty</h1>
            <p>Państwa komentarz (przedstawiony poniżej) został wysłany.</p>
            <!--// nl2br zamienia znaki nowej linii jak \n na znak HTML-->
            <!--nowej linii <br/>-->
            <p><?php echo nl2br(htmlspecialchars($content)); ?></p>
        </div>
    </div>
</div>

</body>
</html>
