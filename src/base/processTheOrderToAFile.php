<!DOCTYPE html>
<html lang="en">
<head>
    <title>My projects</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<!--NAVBAR-->
<nav class="navbar navbar-expand-sm bg-secondary fixed-top">
    <a class="navbar-brand colorNavbar" href="#">Navbar</a>
    <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon "></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ">
            <li class="nav-item ">
                <a class="nav-link whiteLetters" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link whiteLetters" href="frontEnd.php">Front-end</a>
            </li>
            <li class="nav-item">
                <a class="nav-link dOrange" href="backEnd.php">Back-end</a>
            </li>
        </ul>
    </div>
</nav>

<!--CONTAINER-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <nav class="navbar bg-secondary ">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="bakery.html">Piekarnia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dOrange" href="form.html">Zapis do pliku</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link whiteLetters" href="comment.php">Wyślij e-mail</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-sm-10">
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2  ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
            <div class="d-flex ">
                <div class="p-2 ml-auto ">
                    <a href="seeOrders.php" ><button type="button" class="btn btn-info" >Zobacz zamówienia</button></a>
                </div>
                <div class="p-2 ">
                    <a href="form.html" ><button type="button" class="btn btn-info" >Złóż zamówienie</button></a>
                </div>
            </div>
            <h1>Piekarnia</h1>
            <h2>Wyniki zamówienia :</h2>
            <?php

            //krótkie nazwy zmiennych
            $amoundOfBread = $_POST['amoundOfBread'] ?:0;
            $numberOfRolls = $_POST['numberOfRolls'] ?:0;
            $numberOfDonuts = $_POST['numberOfDonuts'] ?:0;
            $source = $_POST['source'];

            $quantity = $amoundOfBread + $numberOfRolls + $numberOfDonuts;

            $dateOfMakingOrder = '<p>Zamówienie przyjęte o '. date('H:i, jS F Y').'</p>';
            if ($dateOfMakingOrder < $quantity){
                echo $dateOfMakingOrder;
            }

            if ($numberOfRolls < 10 )
                $discount = 1;
            elseif ($numberOfRolls >= 10 && $numberOfRolls <= 49 )
                $discount = 0.95;
            elseif ($numberOfRolls >= 50 && $numberOfRolls <= 99)
                $discount = 0.9 ;
            elseif ($numberOfRolls >= 100)
                $discount = 0.85;

            if ($quantity==0){
                echo "Na poprzedniej stronie nie zostało złożone żadne zamówienie! <br/>";
                exit;
            }else
                echo "Zamówiono: </br>";
            {
                if ($amoundOfBread>0)
                    echo htmlspecialchars($amoundOfBread).' chleba <br/>';
                if ($numberOfRolls>0)
                    echo htmlspecialchars($numberOfRolls).' bułek <br/>';
                if ($numberOfDonuts>0)
                    echo htmlspecialchars($numberOfDonuts).' pączków <br/><br/>';
            }
            echo "</br>";

            $value = 0.00;

            define('BREADPRICE',4);
            define ('PRICEOFROLLS',0.7);
            define('PRICEOFDONUTS',1.7);

            $value = $amoundOfBread * BREADPRICE
                + ($numberOfRolls * $discount) * PRICEOFROLLS
                + $amoundOfBread * PRICEOFDONUTS;

            echo "Cena netto: ".number_format($value,2)."PLN<br/>";

            $vatRate = 0.22; // stawka vat wynosi 22%
            $value = $value * (1 + $vatRate);
            echo "Cena brutto: ".number_format($value,2)."PLN</p>";

            echo "Źródło informacji:";
            switch ($source){
                case "a":
                    echo "<p>Stały klient</p>";
                    break;
                case "b":
                    echo "<p>Reklama telewizyjna</p>";
                    break;
                case "c":
                    echo "<p>Książka telefoniczna</p>";
                    break;
                case "d":
                    echo "<p>Znajomy</p>";
                    break;
                default:
                    echo "<p>Źródło nieznane<p/>";
                    break;
            }
            // Zapisywanie do pliku

            $address = $_POST['address'];
            $data=date('H:i, jS F Y');
            $document_root = $_SERVER['DOCUMENT_ROOT'];

            $outputString = $data."\t".$amoundOfBread." chleba \t".$numberOfRolls." bułek\t"
                .$numberOfDonuts." pączków\t".number_format($value,2)
                ."PLN\t". $address."\n";

            $wp = fopen("$document_root/gitlab/my_page/src/base/orders.txt",'ab');

            if (!$wp){
                echo "<p><strong>Zamówienie państwa nie może zostać przyjęte w tej chwili.
        Proszę spróbować później.</strong></p>";
                exit;
            }

            fwrite($wp,$outputString);
            fclose($wp);

            ?>
        </div>
    </div>
</div>

</body>
</html>
