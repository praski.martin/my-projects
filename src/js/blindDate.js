var women = [];
var men = [];
var demo = $('#demo');
var name1 = $('#name1');
var name2 = $('#name2');
var comment1 = $('#comment1');
var comment2 = $('#comment2');

demo.html(women);

$('form').on('submit',function(event) {
    event.preventDefault();
    var woman = name1.val();
    name1.val('');

    if (woman !== null && woman !== '') {
        women.push(woman);
    }

    var man = name2.val();
    name2.val('');
    if (man !== null && man !== '') {
        men.push(man);
    }

    if (women.length <= 4){
        comment1.html("Brakuje jeszcze " + (5 - women.length) + " imion damskich!");
    }else{
        comment1.html("ok");
        name1.prop("disabled", true);
    }

    if (men.length <= 4){
        comment2.html("Brakuje jeszcze " + (5 - men.length) + " imion meskich!");
    }else{
        comment2.html("ok");
        name2.prop("disabled", true);
    }

    if (men.length === 5 && women.length === 5){
        var para = "<ul>";
        for (var i = 0; i<men.length ; i++) {
            var used = Math.floor(Math.random() * women.length);
            para += "<li>" + men[i]+" i " + women[used];
            women.splice(used,1);
        }
        para += "</ul>";
        demo.html("Wylosowane pary : <br>" + para);
        $('#login').addClass("d-none");
    }
});