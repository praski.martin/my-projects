words = ["encyklopedia", "administratorka", "afroamerykanka", "unieruchomiony", "pszczelarz","radioaktywny",
  "rentgenowski"];
var word;
var chars;
var i;
var number_of_windows="";
var windows2="";
var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');

$("button").click(function(){
  var number= Math.floor(Math.random()*words.length);
  word=words[number];

  var word_length = word.length;
  $("h3").html("Wylosowany wyraz ma "+word_length+" liter.<br>Podaj literę.");

  chars=word.split('');

  $('#button').addClass('d-none');
  $('#text').removeClass('d-none');

  for (i=0;i<chars.length;i++){
    number_of_windows += '<span id="' + i + '" class="border border-primary">_</span>';
  };

  $("#frame").html(number_of_windows);
  for (i = 0 ; i < alphabet.length ; i++) {
    windows2 += '<span class="letter border border-danger">' + alphabet[i] + '</span>';
  }
  $("#frame2").html(windows2);
});

var how_many_tries = 0 ;
var good_discovery = 0 ;
$("#frame2").on("click",'.letter', function(){
  how_many_tries++;
  $("#how_many_tries").html("Liczba ruchów : " + how_many_tries );
  var letter =$(this).html();
  for (i = 0 ; i < chars.length ; i++){
console.log(chars);
    if (chars[i]===letter){
      $("#" + i).html(letter);
      $(this).removeClass('letter');
      $(this).addClass('discovered');
      $(this).addClass('bg-success');
      good_discovery++;
      console.log(chars.length);
      if (good_discovery===chars.length){
        $("#button2").removeClass('d-none');
        $('#frame').find('span').each(function () {
          $(this).addClass('bg-primary');
          $(this).addClass('letter_color');
        });
        setTimeout(function () {
          alert("Koniec gry. Twoja liczba ruchów to: " + how_many_tries + " ." );
        },500);
      }
    }else{
      $(this).removeClass('letter');
      $(this).addClass('discovered');
    }
  }
});

var $input = $("input");

$input.on('change', function (){
  var password= $input.val();
  var i = 0;
  if (password===word){
    $('#frame').find('span').each(function () {
      $(this).html(chars[i]);
      i++;
      $(this).addClass('bg-primary');
      $(this).addClass('letter_color');
    });
    setTimeout(function () {
      alert("Koniec gry. Twoja liczba ruchów to: " + how_many_tries + " ." );
    },500);
    $("#button2").removeClass('d-none');
    $(".letter").removeClass('letter');
  }else{
    $input.val("");
    alert("Błędne hasło");
  }
});